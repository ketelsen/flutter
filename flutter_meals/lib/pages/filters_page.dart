import 'package:flutter/material.dart';
import 'package:flutter_meals/widgets/main_drawer.dart';

class FiltersPage extends StatefulWidget {
  static const routeName = '/filters';

  final Function saveFilters;
  final Map<String, bool> currentFilters;
  FiltersPage(this.saveFilters, this.currentFilters);

  @override
  _FiltersPageState createState() => _FiltersPageState();
}

class _FiltersPageState extends State<FiltersPage> {
  bool _glutenFree = false;
  bool _lactoseFree = false;
  bool _vegetarian = false;
  bool _vegan = false;

  @override
  initState() {
    this._glutenFree = widget.currentFilters['glutenFree'];
    this._lactoseFree = widget.currentFilters['lactoseFree'];
    this._vegetarian = widget.currentFilters['vegetarian'];
    this._vegan = widget.currentFilters['vegan'];
    super.initState();
  }

  Widget _buildSwitchListTile({
    String title,
    bool currentValue,
    Function updateValue,
  }) {
    return SwitchListTile(
      title: Text(title),
      value: currentValue,
      onChanged: updateValue,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Filters'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              final selectedFilters = {
                'glutenFree': _glutenFree,
                'lactoseFree': _lactoseFree,
                'vegetarian': _vegetarian,
                'vegan': _vegan
              };
              widget.saveFilters(selectedFilters);
            },
          )
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15),
            child: Text(
              'Filter meals!',
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _buildSwitchListTile(
                  title: 'Glutenfree',
                  currentValue: _glutenFree,
                  updateValue: (newValue) {
                    setState(
                      () {
                        this._glutenFree = !this._glutenFree;
                      },
                    );
                  },
                ),
                _buildSwitchListTile(
                  title: 'Lactosefree',
                  currentValue: _lactoseFree,
                  updateValue: (newValue) {
                    setState(
                      () {
                        this._lactoseFree = !this._lactoseFree;
                      },
                    );
                  },
                ),
                _buildSwitchListTile(
                  title: 'Vegetarian',
                  currentValue: _vegetarian,
                  updateValue: (newValue) {
                    setState(
                      () {
                        this._vegetarian = !this._vegetarian;
                      },
                    );
                  },
                ),
                _buildSwitchListTile(
                  title: 'Vegan',
                  currentValue: _vegan,
                  updateValue: (newValue) {
                    setState(
                      () {
                        this._vegan = !this._vegan;
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
