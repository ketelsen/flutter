import 'package:flutter/material.dart';
import 'package:flutter_meals/widgets/category_item.dart';

import '../dummydata.dart';

class CategoriesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      children: DUMMY_CATEGORIES.map((ct) {
        return CategoryItem(ct.id, ct.title, ct.color);
      }).toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
