import 'package:flutter/material.dart';
import 'package:flutter_meals/dummydata.dart';
import 'package:flutter_meals/models/meal.dart';
import 'package:flutter_meals/pages/categories_page.dart';
import 'package:flutter_meals/pages/category_meals_page.dart';
import 'package:flutter_meals/pages/filters_page.dart';
import 'package:flutter_meals/pages/meal_detail_page.dart';
import 'package:flutter_meals/pages/tabs_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'glutenFree': false,
    'lactoseFree': false,
    'vegetarian': false,
    'vegan': false
  };

  List<Meal> _displayedMeals = DUMMY_MEALS;

  void _setFilters(Map<String, bool> newFilter) {
    setState(
      () {
        _filters = newFilter;

        _displayedMeals = DUMMY_MEALS.where(
          (meal) {
            if (_filters['glutenFree'] && !meal.isGlutenFree) return false;
            return true;
          },
        ).toList();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'A foodie!',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(27, 27, 27, 0.6),
              ),
            ),
      ),
      initialRoute: '/',
      routes: {
        '/': (ctx) => TabsPage(),
        CategoryMealsPage.routeName: (ctx) =>
            CategoryMealsPage(this._displayedMeals),
        MealDetailPage.routeName: (ctx) => MealDetailPage(),
        FiltersPage.routeName: (ctx) =>
            FiltersPage(this._setFilters, this._filters),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('A foodie!'),
      ),
      body: CategoriesPage(),
    );
  }
}
